// Item #2
db.fruits.aggregate([
    {$match: {onSale: true }},
    {$count: "fruitsOnSale"}
])

// Item #3
db.fruits.aggregate([
    {$match: {stock: { $gte: 20}}},
    {$count: "enoughStock"}
])

// Item #4
db.fruits.aggregate([
    {$match: {onSale: true }},
    {$group:{_id: "$supplier_id",
        avg_price: { $avg: "$price"}
        }
    }
])

// Item #5
db.fruits.aggregate([
    {$match: {onSale: true }},
    {$group:{_id: "$supplier_id",
        max_price: { $max: "$price"}
        }
    }
])

// Item #6
db.fruits.aggregate([
    {$match: {onSale: true }},
    {$group:{_id: "$supplier_id",
        min_price: { $min: "$price"}
        }
    }
])