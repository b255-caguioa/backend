// Item #2
db.users.find(
	{$or: 
		[
			{
				firstName: {$regex: 'S'}
			},
			{
				lastName: {$regex: 'D'}
			}
		]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
)



// Item #3
db.users.find({ $and: [{company: "none"}, {age: {$gte: 70}}]})



// Item #4
db.users.find(
	{$and: 
		[
			{
				firstName: {$regex: 'e'}
			},
			{
				age: { $lte: 30}
			}
		]
	}
)