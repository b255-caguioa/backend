console.log("Hello")

// FUnctions
	// Functions in Javascript are lines/blocks of codes that tell our device/application to perform a certain task
	// Functions are mostly created to create complicated tasks to run several lines of code in succession

// Function Declaration

	function printName(){
		console.log("My name is John")
	};

// Function Invocation

	printName();

// Function Declarations vs Expressions
	
	// Function declaration

	// A function can be created through function declaration by using the function keyword and adding a function name

	declaredFunction();

	function declaredFunction(){
		console.log("Hello World from declaredFunction()");
	};

	declaredFunction();

	// Function expression
	// A function can also be stored in a variable. This is called function expression

	// A function expression is an anonymous function assigned to the variableFunction

	// Anonymous function - a function without a name

	let variableFunction = function(){
		console.log("Hello Again!");
	};

	variableFunction();

	// We can also create a function expression of a name function
	// However, to invoke the funcion expression, we invoke it by its variable name, not by its function name

	let funcExpression = function funcName() {
		console.log("Hello from the other sie");
	};

	funcExpression();

	// You can reassign declared functions and function expressions to new anonymous functions

	declaredFunction();

	funcExpression = function(){
		console.log("Updated funcExpression");
	}

	funcExpression();

	// However, we cannot re-assign a function expression initialized with const

	const constantFunc = function(){
		console.log("Initialized with const!")
	}

	constantFunc();

	/*constantFunc = function(){
		console.log("Cannot be reassigned")
	}

	constantFunc();*/


// Function Scoping

/*
	Scope is the accessibility (visiblity) of variables within our program

	Javascript variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/ 

	{
		let localVar = "Armando Perez";
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);

	// Function Scope

	function showNames(){
		var functionVar = "Joe";
		const functionConst = "Jane";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


	// Nested Functions

		// You can create another function inside a function. This is called a nested function.

	function myNewFunction(){
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
		}
		nestedFunction();
	}
	myNewFunction();

	// FUnction and Global Scopred Variables
		// Global Scoped Variables
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";

			console.log(globalName);
		};
		myNewFunction2();

		// console.log(nameInside);

	// Using alert()

		// alert() allows us to show a small window at the top of our browser page to show information to our users as opposed to console.log()

		alert("Hello World");

		function showSampleAlert(){
			alert("Hello, User");
		}
		showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed");


	// Using prompt()
	// prompt() allows us to show a small window at the browser to gather user input. It, much like alert(), will have the page wait until it is dismissed

		let samplePrompt = prompt("Enter your name: ");

		console.log("Hello, " + samplePrompt);

		let sampleNullPrompt = prompt("Don't enter anything.");

		console.log(sampleNullPrompt);
		// Returns an empty string when there is no input. Or null if the user cancels the prompt().

		function printWelcomeMessage(){
			let firstName = prompt("Enter your first name: ");
			let lastName = prompt("Enter your last name: ");

			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		};

		printWelcomeMessage();


		// Function Name Cinventions
		// Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		};

		getCourses();

		// Avoid generic names to avoid confusion

		function get(){
			let name = "Jamie";
			console.log(name);
		};

		get();

		// Avoid pointless and inappropriate function names
		function foo () {
			console.log(25%5);
		};

		foo();

		// Name your functions in small caps. Follow camelCase when naming variables and functions

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		};

		displayCarInfo();


























