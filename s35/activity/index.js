const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://jeraldcaige:eladjheng29@zuitt-bootcamp.sewjcdr.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})


const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res)=> {
	
	User.findOne({username : req.body.username}).then((result, err) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found");

		} else {

			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if(req.body.username !== '' && req.body.password !== ''){
					return res.status(201).send("New user registered");
				} else {
					return res.send("Both username and password must be provided");
				}
			})
		}
	})
})


app.get("/signup", (req, res) => {

	User.find({}).then((result, err) => {

		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app;