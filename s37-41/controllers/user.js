// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if email already exists
/*
	Steps:
	1. Use mongoose "find" method to duplicate emails
	2. Use the "then" method to send a respond back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};

// User Registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) =>{
	// Creates a variabe "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	// Saves the created object to our database
	return newUser.save().then((user, error) =>{
		// User registration failed
		if(error) {
			return false;
		} else {
			return true
		};
	})


}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and retunr false if not
*/

module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the same criteria
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of the comparing login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login from to the encrypted password retrieved from the database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled
				return {access : auth.createAccessToken(result)}
			// Passwords do not match
			} else {
				return false;
			}
		}
	})
}

// S38 ACTIVITY

// Retrieve user details
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Reassign the password of the returned document to an empty string
			3. Return the result back to the frontend
		*/
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		if(result == null){
			return false
		} else {
			result.password = "";

			// Returns the user information with the password as an empty string
			return result;
		}

	});

};

// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database

*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating before returing a response back to the frontend
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false
	}
};
