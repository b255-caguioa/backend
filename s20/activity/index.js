// console.log("Hello World");

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

			// If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the counter number  value is divisible by 5, print/console log the counter number.


function printNumbers(){
	let count = Number(prompt("Enter number: "));
	console.log("The number you provided is " + count);
	while(count > 0){
		
		count--;
		
		if (count <= 50){
			console.log("The current value is at 50. Terminating the loop.");
			break;
		}
		
		if (count % 10 === 0){
			console.log("The number is divisible by 10. Skipping the number");
			continue;
		}
		if (count % 5 === 0){
			console.log(count);
		}
	}
}
count = printNumbers();


//Objective 2
/*let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';*/

//Add code here


let string = 'supercalifragilisticexpialidocious';
let filteredString = string.replace(/[aeiou]/gi, '');

for(let letter = 0; letter < string.length; letter++){
	console.log(string)

	if(string[letter].toLowerCase()){
		continue;
	}
	else {
		break;
	}
}
console.log(filteredString);





//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}
