const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
};

// User Registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) =>{
		if(error) {
			return "Registration failed. Please try again.";
		} else {
			return "User successfully registered."
		};
	})


}

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return "User does not exist.";
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return "Password is incorrect.";
			}
		}
	})
}

// Retrieve user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
			return result;
	});
};


// User product checkout
module.exports.checkout = async (data) => {
	let userUpdated = await User.findById(data.userId).then(users => {
		users.orderedProduct.push({
			product : {
				productId : data.productId,
				quantity : data.quantity
			}
		});

		return users.save().then((users, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	let productUpdated = await Product.findById(data.productId).then(products =>{
		products.userOrders.push({
			userId : data.userId
		});
		
		return products.save().then((products, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	if(userUpdated && productUpdated){
		return "Product successfully added to cart.";
	} else {
		return "Failed to add to cart."
	}
};


// Make user an admin
module.exports.adminUser = (reqParams) => {

	let updateUserAdmin = {
		isAdmin : true
	};
	return User.findByIdAndUpdate(reqParams.userId, updateUserAdmin).then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};


// Retrieves all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
}


// Retrieves all admins
module.exports.getAllAdmins = () => {
	return User.find({isAdmin : true}).then(result => {
		return result;
	})
}


