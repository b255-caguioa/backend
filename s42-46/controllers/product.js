const Product = require("../models/Product");

// Creates a new product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newProduct.save().then((product, error) => {
		// Product creation failed
		if(error) {
			return false;
		// Product creation successful
		} else {
			return true
		}
	})
}


// Retrieves all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


// Retrieves all active products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


// Retrieving a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
}


// Updating a product information
module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		isActive : reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


// Archiving a product
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};


// Activating a product
module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};