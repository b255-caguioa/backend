const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


// Route for creating a new product
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.addProduct(req.body).then(resultFromController =>
			res.send("Product creation successful."))
	} else{
		res.send("Request invalid. Product creation is limited only to admin account.");
	}
});


// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all the active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})


// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send("Product updated successfully."))
	} else{
		res.send("Request invalid. Product update is limited only to admin account.");
	}
});


// Route for archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.archiveProduct(req.params).then(resultFromController => res.send("Product successfully archived."))
	} else{
		res.send("Request invalid. Archiving a product is limited only to admin account.");
	}
});

// Route for activating a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.activateProduct(req.params).then(resultFromController => res.send("Product successfully activated."))
	} else{
		res.send("Request invalid. Activating a product is limited only to admin account.");
	}
});






module.exports = router;