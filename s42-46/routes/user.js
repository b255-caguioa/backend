const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for checking if the user's email already exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
});


// Route for user checkout
router.post("/checkout", auth.verify, (req, res) => {
let data = {
	userId : auth.decode(req.headers.authorization).id,
	productId : req.body.productId,
	quantity : req.body.quantity
}
	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

// Route for creating an admin
router.put("/:userId/makeadmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		userController.adminUser(req.params).then(resultFromController => res.send("User successfully created as admin."))
	} else{
		res.send("Request invalid. Creating an admin is limited only to admin account.");
	}
});

// Route for retrieving all user
router.get("/all", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all admins
router.get("/admins", (req, res) => {
	userController.getAllAdmins().then(resultFromController => res.send(resultFromController))
});


module.exports = router;