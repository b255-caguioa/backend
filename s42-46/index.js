const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");



const app = express();

mongoose.connect("mongodb+srv://jeraldcaige:eladjheng29@zuitt-bootcamp.sewjcdr.mongodb.net/?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
		console.log(`API is now online on port ${ process.env.PORT || 4000}`)
	});
}

module.exports = app;