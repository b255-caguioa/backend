const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required : [true, "Product name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type : Object,
				required : [true, "User Id is required"]
			},
			orderId : {
				type : String,
				default : true
			}
		}
	] 
})

module.exports = mongoose.model("Product", productSchema);