const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProduct : [
		{
			product : [
				{
					productId : {
						type : Object,
						required : [true, "Product ID is required"]
					},
					productName : {
						type : String
					},
					quantity : {
						type : Number,
						required : [true, "Quantity is required"]
					}
				}
			],
			totalAmount : {
				type : Number
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);

