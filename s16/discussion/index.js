console.log("Hello World!");

// Arithmetic operators

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);


// Assignment Operators

	// Basic assignment operator (=)
	let assignmentNumber = 8;

	// Addition assignment operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);


	// Shorthand
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber -= 2;
	console.log("Result of addition subtraction operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of addition multiplication operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of addition division operator: " + assignmentNumber);


// Multiple Operators and Parenthesis

	/*
		-When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction)
	*/

	let	mdas = 1 + 3 - 3 * 4 / 5;
	console.log("Result of mdas operations: " + mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("result of pemdas operation: " + pemdas);

// Increment and Decrement
	let z = 1;

	let increment = ++z;
	console.log("Result of increment " + increment);

	let decrement = --z;
	console.log("Result of decrement " + decrement);


// Type coercion
	/*
		- Type coercion is the automatic or implicit conversion of values from one data type to another
		- This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
		- Values are automatically converted from one data type to another in order to resolve operations
	*/ 

	let numA = '10';
	let numB = 12;

	// Adding/concatenating a string and a number will result in a string

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);


// Comparison Operators

	let juan = "juan";

	// Equality Operator(==)
	/*
		- Checks whether the operands are equal/have the same content
		- Returns a boolean value
	*/ 

	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	// Compares two strings that are the same
	console.log('juan' == 'juan');
	// Compares a string with the variable "juan" declared above
	console.log('juan' == juan);

	// Inequality operator
	/*
		- Checks whether the operands are not equal/have different content
	*/ 

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);

	// Strict Equality Operator
	/*
		- Checks whether the operans are equal/have the same content
		- Also COMPARES the data types of 2 values
		- Javascript is a loosely typed language meaning tha values of different data types can be stored in variables
		- This sometomes can cause problems within our code
	*/ 

	console.log("START OF STRICT EQUALITY OPERATOR")
	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === 'false');
	console.log('juan' === 'juan');
	console.log('juan' === juan);

	// Strict Inequality Operator

	console.log("START OF STRICT INEQUALITY OPERATOR")
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== 'false');
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

// Relational Operators

	// Some comparison operators check whether one value is greater or less than the other value

	let a = 50;
	let b = 65;

	// GT or Greater Than operator (>)
	let isGreaterThan = a > b;
	// LT or Less Than operator (<)
	let isLessThan = a < b;
	// GTE or Greater Than or Equal to (>=)
	let isGTorEqual = a >= b;
	// LTE or Less tha or Equal to (<=)
	let isLTorEqual = a <= b;

	console.log("START OF RELATIONAL OPERATIONS")
	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);


// Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical And Operator (&& - Double Ampersand)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);

	// Logical OR operator (|| - Double Pipe)
	// Returns true if one of the operands are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT operator (! - Exclamation Point)
	// Returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);



































