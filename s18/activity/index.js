/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/
// console.log("Hello world!");

// 1. addNum
	function addNum(num1, num2){
		console.log("Displayed sum of 5 and 15")
		console.log(num1 + num2);
	};
	addNum(5,15);
	


// 2. subNum
	function subNum(num1, num2){
		console.log("Displayed difference of 20 and 5")
		console.log(num1 - num2);
	};
	subNum(20,5);



// 3. multiplyNum
	function multiplyNum(num1, num2) {
		let product = (num1 * num2);
		console.log("The product of 50 and 10:");
		console.log(product);
	};
	multiplyNum(50, 10); 



// 4. divideNum
	function divideNum(num1, num2){	
		let quotient = (num1 / num2);
		console.log("The quotient of 50 and 10:");
		console.log(quotient);
	};
	divideNum(50, 10); 



// 5. getCircleArea
	function getCircleArea(circleArea){
		console.log("The result of getting the area of a circle with 15 radius:")
		console.log(Math.round(circleArea * circleArea * Math.PI * 100) / 100);
	};
	getCircleArea(15);



// 6. getAverage
	function getAverage(num1, num2, num3, num4){
		console.log("The average of 20, 40, 60 and 80:")
		let averageVar = (num1 + num2 + num3 + num4);
		let	length = 4;
		console.log(averageVar / length);

	};
	getAverage(20,40,60,80);



// 7. checkIfPassed
	function checkIfPassed(score1, score2){	
		let isPassed = (score1 / score2) * 100;
		let isPassingScore = isPassed >= 75;
		console.log("Is 38/50 a passing score?");
		console.log(isPassingScore);
	};
	checkIfPassed(38,50); 







//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
