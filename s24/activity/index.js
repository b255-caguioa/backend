// Exponent Operator
let getCube = 2 ** 3;

// Template Literals
console.log(`The cube of 2 is ` + getCube);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`)


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
console.log(`${animal.name} was a ${animal.species}. He weighed ${animal.weight} with a measurement of ${animal.measurement}.`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((num) => {
	console.log(num)
});

let reduceNumber = numbers
const initial = 0;
const sum = reduceNumber.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  initial
);
console.log(sum);


// Javascript Classes
class Dog{
	constructor(name, age, breed){
		this.name = "Frankie";
		this.age = 5;
		this.breed = "Miniature Dachshund";
	}
}
const myDog = new Dog();
console.log(myDog);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}
