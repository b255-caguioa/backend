console.log("Hello World")

// [SECTION] Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write strins without using the concatenation operator (+)
	- Greatly helps with code readability
*/

let name = "John";

// Pre-template literal string
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals:" + message);

// Template literals
message = `Hello ${name}! Welcome to programming!`
console.log(`Message with template literals: ${message}`)

// Multi-line using template literals
const anotherMessage = `
${name} attended a math competition.
he won it by solving the problem 8**2 with the solution of ${firstNum}.
`
console.log(anotherMessage);

/*
	-Template literals allow us to write strings with embedded JavaSript expressions
	-Expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals

*/
const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring
/*
	-Allows to unpack elements in arrays into distinct variables
	-Allows us to name array elements with variables instead of using index numbers
	-Helps with code readability
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} It's nice to meet you!`)

// Array destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// [SECTION] Object Destructuring
/*
	-Allows to unpack properties of objects into distinct variables
	-Shortens the syntax for accessing properties from objects
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);


// Object Destructuring
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(middleName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! Its good to see you again.`)

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);

}

getFullName(person);


// [SECTION] Arrow Functions
/*
	-Compact alternative syntax to traditional functions
	-Useful for code snippets where creating functions will not be reused in any other portion of the code
*/

const hello = () => {
	console.log("hello world");

}

// Pre-arrow function
function functionName(parameterA, parameterB, parameterC){
	console.log();
}

// Arrow function
let variableName = (parameterA, parameterB, parameterC) => {
	console.log();
}

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D.", "Smith");

const students = ["John", "Jane", "Judy"];

// Pre arrow function
students.forEach(function(students){
	console.log(`${students} is a student`)
})

// Arrow function
students.forEach((students) => {
	console.log(`${students} is a student`)
})

// [SECTION] Implicit return
/*
	-There are instances when you can omit the "return" statement
	-This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
*/

const add = (x,y) => x+y;

let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value
/*
	-Provides a default argument if none is provided when the function is invoked
*/

const greet = (name = 'User') => {
	return `Good Morning, ${ name}! `
}

console.log(greet());
console.log(greet('John'));

// [SECTION] Class-based Object Blueprints
/*
	-Allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;

	}
}

// Initializing an object
/*
	-The "new" operator creates/instantiates a new object with the given argument as the values of it's properties
	- No arguments provided will create an object without any values assigne to its properties
*/

const myCar = new Car();

console.log(myCar);

// Values of properties may be assigned after creation.instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";

console.log(myCar);

// Creating/Instantiating a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", "2021");

console.log(myNewCar);

