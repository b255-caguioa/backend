const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://jeraldcaige:eladjheng29@zuitt-bootcamp.sewjcdr.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	} 
);

app.use("/tasks", taskRoute);

if(require.main === module){
	app.listen(port, () => console.log(`Server Running at ${port}`))
}
module.exports = app;