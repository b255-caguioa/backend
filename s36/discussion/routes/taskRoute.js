// Contains all the endpoints of our application
// We separate the routes such that "app.js" only contains information on the server
// We need to use the express Router() function to achieve this
const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// the "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController");

// [SECTION] Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when the route is accessed
// They invoke the controller functions from the controller files

// Route to get all the tasks
// This route expects to receive a GET request at the URL "/tasks"
router.get("/", (req, res) => {

	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result to postman
	// "resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name for a result using the parameter name "result/res"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})


// Route to delete a task
// The task id is obtained from the URL is denoted by the  ":id" indetifier in the route
// the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;