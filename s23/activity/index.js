// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added

// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon

// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object


let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!");
	}
}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer.pokemon);

console.log("Result of talk method");
trainer.talk();


function pokemon(name, level, health, attack, tackle){
	this.name = name;
	this.level = level;
	this.health = 2*attack;
	this.attack = attack;
	this.tackle = level;


	this.tackle = function(target){
		target.health  = target.health - this.attack
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is reduced to " + target.health);

	};
	this.faint = function(){
		console.log(this.name + ' fainted');
	};

};
pokemon();

let pikachu = new pokemon("Pikachu", 12, 24, 12);
console.log(pikachu);

let geodude = new pokemon("Geodude", 8, 16, 8);
console.log(geodude);

let mewtwo = new pokemon("Mewtwo", 100, 200, 100);
console.log(mewtwo);



geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
geodude.faint();
console.log(geodude);

















//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
