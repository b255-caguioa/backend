console.log("Hello world!");

// [SECTION] Objects
/*
	- AN object is a data type that is used to represent real world objects. 
	- It is a collection of related data and/or functionalities
	- In Javascript, most core Javascript features like strings and arrays are objects(Strings are a collection of characters and arrays are a collection of data)
	- Information stored in objects are represented in a "key:value" pair
	- A "key" is also mostly referred to as a "property" of an object
	- Different data types may be stored in an object's property creating complex data structures
*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);
console.log(cellphone.manufactureDate);


// Creating objects using a constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructors function parameters
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}
// The "new" operator creates an instance of the object
let laptop = new Laptop ('Lenovo', 2008);
console.log('Result from creating objects using object constructors');
console.log(laptop);

let myLaptop = new Laptop ('Macbook Air', 2020);
console.log('Result from creating objects using object constructors');
console.log(myLaptop);



// Creating empty objects
let computer = {};
let myComputer = new Object();



// [SECTION] Accessing Object properties

let array = [laptop, myLaptop];
// May be confused for accessing array indexes
console.log(array[0]['name']);
// This tells us that array[0] is an object by using the dot notation
console.log(array[0].name);



// [SECTION] Initializin/Adding/Deleting/Reassigning Object Properties
/*
	- Like any other variable in Javascript, objects may have their properties initialized/added after the object was created/declared
	- This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};

// Initializing/adding object properties using dot notation
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);
console.log(car.name);

// Initializing/adding object properties using square bracket notation
/*
	- While using the square bracket, this will allow access to spaces when assigning names to make it easier to read. This also makes it so that object properties can only be accessed using the square bracket notation
*/
car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log("Result from adding properties using square bracket notation");
console.log(car);


// Deleting object properties
delete car['manufacture date'];
console.log('Result from deleting properties');
console.log(car);


// Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties');
console.log(car);



// [SECTION] Object Methods
/*
	- A method is a function which is a property of an object
	- They are also functions and one of the key differences they have is that methods are functions related to a specific object
	- Methods are useful for creating object specific functions which are used to perform tasks on them
*/

let person = {
	name: "John",
	talk: function(){
		console.log('Hello. My name is ' + this.name);
	}
}
console.log(person);
console.log('Result from object methods:');
person.talk();

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' Walked 25 steps forward');
}
person.walk();


// Mini Activity
// Add a method to the person object
// That method should be able to console.log a hobby being done by John
// Use the "this" keyword to access the name John


person.hobby = function(){
	console.log("One of " + this.name +"'s hobbies is painting.");
}
person.hobby();



// Methods are useful for creating reusable functions that perform tasks related to objects
let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		state: "Texas"
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log("Hello. My name is  " + this.firstName + ' ' + this.lastName);
	}
};
friend.introduce();


// [SECTION] Real world applicaiton of objects
/*
	- Scenario
		1. We would like to create a game that would have several Pokemon interact with each other
		2. Every Pokemon would have the same set of stats, peroperties and functions
*/

// Using object literals to create multiple kinds of Pokemon would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor instead will help with the process
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + 'fainted');
	}
};

// Created new instances of the new "Pokemon" objects each with their unique properties

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);





























































