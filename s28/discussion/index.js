// CRUD Operations

// Inster Document (CREATE)

/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Document
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"company": "none"
});

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 26,
		"email": "stephenhawking@mail.com",
		"company": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"company": "none"
	}
]);


//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": true
	}

]);

// Find Documents(Read/Retrieve) ----------------------------------------

// db.users.find(); This will retrieve all the documents in the collection


db.users.find({
	"firstName": "Jane"
});

db.users.findOne({}); //Returns the first document in our collection

db.users.findOne({
	"firstName": "Stephen"
});

// Update Document -------------------------------------------------

db.users.insertOne({
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"email": "test@mail.com",
		"company": "none"
});

// Updating one document
db.users.updateOne(
	{	
		"firstName":"Test"
	},
	{
		$set:{

		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"email": "billgates@mail.com",
		"department": "Operations",
		"status": "active"
		}
	}
);



// Removing a field -------------------------------------------------
db.users.updateOne(
	{	
		"firstName": "Bill"
	},

	{
		$unset:{
			"status": "active"
		}
	}
);

// Updating Multiple Documents -------------------------------------------------
db.users.updateMany(
	{	
		"company": "none"
	},

	{
		$set:{
			"company": "HR"
		}
	}
);

db.users.updateOne(
	{},

	{
		$set:{
			"company": "Operations"
		}
	}
);

db.users.updateMany(
	{},

	{
		$set:{
			"company": "comp"
		}
	}
);


// Deleting Documents (Delete) -------------------------------------------------

db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({
	"firstName": "Test"
});

db.users.deleteMany({
	"company": "comp"
});

db.courses.deleteMany({})



















