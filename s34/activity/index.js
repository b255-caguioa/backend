const express = require("express")

const app = express()
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));



// Item no. 1 (home route)
app.get("/home", (req, res) => {
	res.send("Welcome to the homepage")
});


// Item no. 3 (get users)
let users = [
	{	
		"username": "johndoe",
		"password": "johndoe1234"
	}
]
app.get("/users", (req, res) => {
	res.send(users)
});


// Item no. 5 (delete user)
app.delete("/delete-user", (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if (req.body.username == users[i].username){

			message = `User ${req.body.username} has been deleted.`;

			break;
		} else {
			message = "User does not exist."
		}
	}
	res.send(message);
	console.log(users);

})











if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}
module.exports = app;
