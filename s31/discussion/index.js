// "require" directive - used to load node.js modules
// "http module" - lets the node.js transfer data using the HTTP
// "HTTP" - protocol that allows the fetching of resources
/*
	We are now able to run a simple node.js server. Wherein when we added our URL in the browser, the browsers client actually requested to our server and our server was able to respond with a text

	We use require() method to load node.js modules
		A module is a software component or part of a program which contains one or more routines

		The HTTP module is a default module from node.js

		The HTTP module let node.js transfer data or let our client and server exchange data via hypertext transfer protocol
*/

let http = require("http");

/*
	http.createServer() method allows us to create a server and handle the request of a client

	request - messages sent by the client, usually via a web browser

	response - messages sent by the server as an answer
*/

http.createServer(function(request, response){
	
	// res.writeHead is a method of the response object. This will allows us to add headers, which are additional information about our server's response. 'Content-type' is one of the more recognizable headers, it is pertaining to the data type of the content we are responding with. The first argument in writeHead is an HTTP which is used to tell the client about the status of thier request. 200 eaning OK. HTTP 404 means the resource you're trying to access cannot be found. 403 means the resource you're trying to access is forbidden or requires authentication
	response.writeHead(200, {"Content-Type": "text/plain"});
	
	// res.end() is a method of the response object which ends the server's response and sends a message/data as a string
	response.end("Hello World");

// .listen() allows us to assign a port to a server.
// port is a virtual point where connections start and end.
// http://localhost:4000 - localhost is your current and 4000 is the port number is assigned to where the process/server is listening or running from. Port 4000 - popularly used for backend applications
}).listen(4000)

// When server is running, console will print out the message.
console.log('Server is running at localhost:4000');















































