// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "HTTP module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients(browser) and servers(nodejs/express js applications) communicate by exchanging individual messages
// The messages ent by the client, usually a web browser, are called requests
// The messages sent by the server as an answer are called responses

let http = require("http");

http.createServer(function(request, response){
	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
	// The method "GET" means that we will be retrieving or reading information
	if(request.url == "/items" && request.method == "GET"){

		// Requests the "/items" path and "GETS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// Ends the response process
		response.end('Data retrieved from the database');
	}

	// The method "POST" means that we will be adding or creating information
	// In this example, we will just be sending a text repsponse for now
	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database');
	}
}).listen(4000);

console.log('Server is running at localhost:4000');