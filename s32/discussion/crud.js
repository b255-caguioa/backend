let http = require("http");

// Mock database
let directory = [
	{	
		"name": "Brandon",
		"email": "brandon@gmail.com"
	},
	{	
		"name": "Jobert",
		"email": "jobert@gmail.com"
	}
]

http.createServer(function(request, response){

	// Route for returning all items upon receiving a GET request
	if(request.url == "/users" && request.method == "GET"){
		// Sets response output to JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'});
		// Input HAS to be data type STRING hence the JSON.stringify() method
		// This string input will be converted to the desired output data type which has been sent to JSON
		// This is done because requests and responses sent between client and Node.js server requires the information sent and received as a strinigified JSON
		response.write(JSON.stringify(directory));
		response.end();
	}

	// A request object contains several parts
		// - Headers - contains information about the request context/content like what is the data type
		// - Body - contains the actual information being sent with the request
	if (request.url == "/users" && request.method == "POST"){

		let requestBody = '';

		// Data is received from the client and is processed in the data stream
		// The information provided from the equest object enters a sequence called "data" the code below will be triggered
		request.on('data', function (data){
			// Assigens the data retrieved from the data stream to the requestBody
			requestBody += data;
		});

		request.on('end', function(){
			// We need this to be of data type JSON to access its properties
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})

	}

}).listen(4000);

console.log('Server running at localhost:4000');






















































