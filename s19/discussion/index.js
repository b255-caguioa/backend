console.log("Hello world!")

// Conditional Statements allows us to control the folow of our program. TI allows us to run a statement/instructions if a condition is met or run another separate instruction if otherwise.

// [SECTION] if, else if, else statement
let numA = -1;

if(numA < 0 ){
	console.log("Hello");
}

// THe result of the expression added in the if's condition must result to true, else, the statement if() will not run.

// You can also check the condition. The expression results to a boolean true because of the use of the less than operator.
console.log(numA<0);
// results to true and so the if statement was run.

numA = 0;
if(numA < 0){
	console.log("Hello again if numA is 0!");
}

// It will not run because the expression now results to false.
console.log(numA < 0);

let city = "New York"

if (city === "New York") {
	console.log("Welcome to New York City!");
}

// Else if clause

/*
	- Executes a statement if previous conditions are false and if the specified conditions are true
	- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/

let numH = 1;

if(numA < 0){
	console.log('Hello');
} else if (numH > 0){
	console.log("World");
}

// We were able to run the else if() statement after we evaluated that the if condition was failed

// If the if() condition was passed and run, we will no longer evaluate the else if() and end the process there.

numA = 1;

if (numA > 0){
	console.log('Hello');
} else if (numH > 0){
	console.log('World');
}

// else if() statement no longer ran because of the if statement 


city = "Tokyo";

if (city === "New York") {
	console.log("Welcome to New York City!");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan!");
}

// Since we failed the condition for the first if(), we went to the else if() and checked and passed that condition

/*
	- Executes a statement if all other conditions are false
	- The "else" statement is optional and can be added to capture any other result to change the flow of the program
*/

if (numA < 0) {
	console.log('Hello');
} else if (numH === 0){
	console.log('World');
} else {
	console.log('Error! numA is not less than 0')
}

// Else statements should only be added if there is a preceeding if condition. Else statements by itself will not work, however, if statements will work even if there is no else statement.

// if, else if and else statements with functions 
/*
	- Most of the time we would like to use if, else if and else statements with functions to control the flow of our application
	- By including them inside functions, we can decide when certain conditions will be checked instead of executing statements when the Javascript loads
	- The return statement can be utilized with conditional statements in combination with functions to change values to be used for other features
*/

let message = 'No Message';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet';
	}
	else if (windSpeed <= 61){
		return 'Tropical depression detected';
	}
	else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected'
	}
	else if (windSpeed >= 89 || windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected';
	}
}
message = determineTyphoonIntensity(62);
console.log(message);

// MINI-ACTIVITY
// Create a function with an if else statement inside
// The function should test for if a given number is an even number or an odd number.

// Print "Number is even" if the number is even//divisible by 2
// Else print "Number is odd" if the number has a remainder when divided by 2.

/*function determineNumber(number){
	if (number % 2 === 0);{
		return 'Number is even';
	} 
	else{
		return 'Number is odd';
	}
}
message = determineNumber(10)
console.log(message);
*/



// Truth Examples
/*
	- If the result of an expression in a condition results to a truthy value, the condition returns true ant the corresponding statements are excuted
	- Expressions are any unit of code that can be evaluated to a value
*/

if (true) {
	console.log('Truthy');
}
if (1) {
	console.log('Truthy');
}

if ([]) {
	console.log('Truthy');
}

// Falsy examples
if (false) {
	console.log('Falsy');
}
if (0) {
	console.log('Falsy');
}
if (undefined) {
	console.log('Falsy');
}

// [SECTION] Conditional (ternary) Operator
/*
	- The cinditional (Ternary) Operator takes in three operands
	1. condition
	2. expression to execture if the condition is truthy
	3. expression to execute if the condition is falsy
	- Can be used as an alternative to an "if else" statement
	- Commonly used for single statement execution where the result consists of only one line of code

	Syntax:
	(Expression) ? ifTrue : ifFalse
*/

// Single statement execution
let ternaryResult = (1<18) ? true : false
console.log("Result of Ternary operator: " + ternaryResult);

// Multi statement execution
let name;

function isOfLegalAge(){
	name = 'John'
	return 'You are of the legal age limit'
}
function isUnderAge(){
	name = 'Jane'
	return 'You are under the age limit'
}

/*
	- input received from the prompt function is returned as a string data type
	- the "parseInt" function converts the input received into a number data type.
*/

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name)


// [SECTION] Switch Statements

/*
	- The switch statement evaluates an expression and matches the expressions value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the match

	- Switch cases are considered as "loops" meaning it wil compare the expression with each of the case values until a match is found
	- The "break" statement is used to terminated the current loop once a match has been found
*/

let day = prompt("What day of the week is it today?")
	.toLowerCase();
	console.log(day);

switch (day) {
	case 'monday' :
		console.log("The color of the day is red");
		break;
	case 'tuesday' :
		console.log("The color of the day is orange");
		break;
	case 'wednesday' :
		console.log("The color of the day is yellow");
		break;
	case 'thursday' :
		console.log("The color of the day is green");
		break;
	case 'friday' :
		console.log("The color of the day is blue");
		break;
	case 'saturday' :
		console.log("The color of the day is indigo");
		break;
	case 'sunday' :
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input valid day")
		break;
}

// [SECTION] Try-catch-finally statement
/*
	- "Try-catch" statements are commonly used for error handling
	- There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
	- These errors are a result of an attempt of the programming language to help developers in creating efficient code.
	- They are used to specify a response whenever an exception/error is received
*/

function showIntensityAlert (windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));

	// error/err are commonly used variable names used by developers for storing errors
	} catch(error) {
		// The typeof operator is used to check the data type of a value/expression and returns a string value of what the data type is 
		console.log(typeof error);

		// catch errors within the try statement
		// In this case, the error is an unknown function 'alerat' which does not exist in JavaScript
		// "error.message" is used to access the information relating to an error object

		console.warn(error.message);
	} finally {

		// Continue executio of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors
		alert('Intensity updates will show new alert');
	}
}
showIntensityAlert(56);
































