/*
    
    1. Create a function called login which is able to receive 3 parameters called username,password and role.
        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
            -if it is, return a message in console to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."
*/
let username;
let password;
let role;

/*
    usernames & roles:

    adminUser = admin
    teacherUser = teacher
    studentUser = student
*/

/*
function login(username, password, role){

    username = prompt('Enter username: ');
    password = prompt('Enter password: ');
    role = prompt('Enter role: ').toLowerCase();

    console.log("login: " + username + ', ' + password + ', ' + role);

    if(username == '' || password == '' || role == ''){
        console.log("Inputs should not be empty");
    } else {
        switch(username, password, role){
        case 'adminUser' && 'password' && 'admin':
            console.log("Welcome back to the class portal, admin!");
            break;
        case 'teacherUser' && 'password' && 'teacher':
            console.log("Thank you for logging in, teacher!");
            break;
        case 'studentUser' && 'password' && 'student':
            console.log("Welcome to the class portal, student!");
            break;
        default:
            console.log("Role out of range.");
        }
    }
}
login(username, password, role);
*/


/*
    2. Create a function called checkAverage able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

    Note: strictly follow the instructed function names.
*/

let grade1 = prompt('Enter 1st Grade:');
let grade2 = prompt('Enter 2nd Grade:');
let grade3 = prompt('Enter 3rd Grade:');
let grade4 = prompt('Enter 4th Grade:');

function checkAverage(grade1, grade2, grade3, grade4){
    console.log("checkAverage: " + grade1 + ', ' + grade2 + ', ' + grade3 + ', ' + grade4);
    let average = (grade1 + grade2 + grade3 + grade4) / 4;
    console.log(Math.round(average * average * Math.PI * 100) / 100);
};
checkAverage(grade1,grade2,grade3,grade4);


   
function average(checkAverage){

    if (checkAverage <= 74){
        return "Hello, student, your average is " + checkAverage + ". The letter equivalent is F";
    }
    else if (checkAverage >= 75 && checkAverage <= 79){
        return "Hello, student, your average is " + checkAverage + ". The letter equivalent is D";
    }
    else if (checkAverage >= 80 || checkAverage <= 84){
        return "Hello, student, your average is  " + checkAverage + ". The letter equivalent is C";
    }
    else if (checkAverage >= 85 || checkAverage <= 89){
        return "Hello, student, your average is " + checkAverage + ". The letter equivalent is B";
    }
    else if (checkAverage >= 90 || checkAverage <= 95){
        return "Hello, student, your average is " + checkAverage + ". The letter equivalent is A";
    }
    else{
        return "Hello, student, your average is " + checkAverage + ". The letter equivalent is A+";
    }
}

message = average(checkAverage / 4);
console.log(message);



//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}
