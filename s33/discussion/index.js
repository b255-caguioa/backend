console.log("Hello World!");

// [SECTION] Getting all posts 

// The Fetch API allows you to asynchronously request for a resource (data)

// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value 

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));


// Retrieves all posts following the REST API 
// By using the then method we can now check for the status of the promise 
fetch('https://jsonplaceholder.typicode.com/posts')
// the 'fetch' method will return a promise that resolves to a response object
// the "then" method captures the response object and returns another promise which will be eventually resolved or rejected
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request
// Using multiple "then" methods creates a "promise chain"
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code 
// Used in functions to indicate which portions of code should be waited for 
// Creates an asynchronous function
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the result vasriable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result returned by fetch returns a promise
	console.log(result);
	// the returned response is an object
	console.log(typeof result);
	// We cannot access the content of response by directly accessing its body property 
	console.log(result.body);

	// Converts the data from the "response" object as JSON
	let json = await result.json();
	// Print out the content of the "Response" object
	console.log(json);
}

fetchData();



// [SECTION] Getting a specific post 

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Creating a post 
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method ofthe request object to POST following the REST API 
	// Default method is get 
	method: 'POST',
	// Sets the header data of the request object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: { 
		'Content-Type': 'application/json',
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({ 
		title: 'New Post',
		body: 'Hello World',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',

	headers: { 
		'Content-Type': 'application/json',
	},

	body: JSON.stringify({ 
		id: 1,
		title: 'Updated Post',
		body: 'Hello Again',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post using PATCH 

// Updates a specific post following the REST API 
// The difference between PUT and PATCH is the number of properties being changed 
// Patch is used to update the whole object 
// PUT is used to update a single/several properties 

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',

	headers: {
		'Content-Type': 'application/json',
	},

	body: JSON.stringify({ 
		title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

