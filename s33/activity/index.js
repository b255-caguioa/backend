// Item No. 3
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'GET', 
	headers: {
		'Content-type': 'application/json',
	},
})
.then((response) => response.json())
.then((json) => console.log(json));


// Item No. 4
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map((mapItem) => {return mapItem.title})));


// Item No. 5
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


// Item No. 6
